<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'קבצים';

$string['sitefilesloaded'] = 'Site files loaded';
$string['bytes'] = 'בתים';
$string['cannoteditfolder'] = 'You do not have permission to add content to this folder';
$string['changessaved'] = 'Changes saved';
$string['contents'] = 'תוכן';
$string['copyrightnotice'] = 'Copyright notice';
$string['create'] = 'יצירה';
$string['Created'] = 'נוצר';
$string['createfolder'] = 'יצירת תיקייה';
$string['Date'] = 'תאריך';
$string['defaultquota'] = 'מכסת אחסון';
$string['defaultquotadescription'] = 'You can set the amount of disk space that new users will have as their quota here. Existing user quotas will not be changed.';
$string['deletefile?'] = 'Are you sure you want to delete this file?';
$string['deletefolder?'] = 'Are you sure you want to delete this folder?';
$string['Description'] = 'תאור';
$string['destination'] = 'תאור';
$string['Download'] = 'הורדה למחשב';
$string['downloadoriginalversion'] = 'הורידו את הגירסה המקורית';
$string['editfile'] = 'עריכת קובץ';
$string['editfolder'] = 'עריכת תיקייה';
$string['emptyfolder'] = 'תיקייה ריקה';
$string['file'] = 'קובץ';
$string['File'] = 'קובץ';
$string['filealreadyindestination'] = 'The file you moved is already in that folder';
$string['files'] = 'קבצים';
$string['Files'] = 'קבצים';
$string['fileexists'] = 'הקובץ קיים';
$string['fileexistsonserver'] = 'A file with the name %s already exists.';
$string['fileexistsoverwritecancel'] =  'A file with that name already exists.  You can try a different name, or overwrite the existing file.';
$string['filelistloaded'] = 'File list loaded';
$string['filemoved'] = 'File moved successfully';
$string['filenamefieldisrequired'] = 'The file field is required';
$string['fileinstructions'] = 'העלו את התמונות, מסמכים, קבצים שונים לשם שיבוץ בתיקי-התלמיד שלכם. לשם העברה של קובץ או תיקייה יש לגרור ולשחרר אותה על מיקום חדש';
$string['filethingdeleted'] = '%s נמחק';
$string['folder'] = 'תיקייה';
$string['Folders'] = 'תיקיות';
$string['foldercreated'] = 'תיקייה נוצרה';
$string['groupfiles'] = 'קבוצת קבצים';
$string['home'] = 'ראשי';
$string['htmlremovedmessage'] = 'You are viewing <strong>%s</strong> by <a href="%s">%s</a>. The file displayed below has been filtered to remove malicious content, and is only a rough representation of the original.';
$string['htmlremovedmessagenoowner'] = 'You are viewing <strong>%s</strong>. The file displayed below has been filtered to remove malicious content, and is only a rough representation of the original.';
$string['image'] = 'תמונה';
$string['lastmodified'] = 'עודכן לאחרונה';
$string['myfiles'] = 'הקבצים שלי';
$string['Name'] = 'שם';
$string['namefieldisrequired'] = 'The name field is required';
$string['movefaileddestinationinartefact'] = 'You cannot put a folder inside itself.';
$string['movefaileddestinationnotfolder'] = 'You can only move files into folders.';
$string['movefailednotfileartefact'] = 'Only file, folder and image artefacts can be moved.';
$string['movefailednotowner'] = 'You do not have permission to move the file into this folder';
$string['movefailed'] = 'Move failed.';
$string['nofilesfound'] = 'No files found';
$string['overwrite'] = 'Overwrite';
$string['Owner'] = 'Owner';
$string['parentfolder'] = 'Parent folder';
$string['Preview'] = 'תצוגה מקדימה';
$string['savechanges'] = 'Save changes';
$string['Size'] = 'גודל';
$string['timeouterror'] = 'File upload failed: try uploading the file again';
$string['title'] = 'שם';
$string['titlefieldisrequired'] = 'The name field is required';
$string['Type'] = 'סוג';
$string['unlinkthisfilefromblogposts?'] = 'This file is attached to one or more blog posts.  If you delete the file, it will be removed from these posts.';
$string['upload'] = 'העלאה';
$string['uploadexceedsquota'] = 'Uploading this file would exceed your disk quota. Try deleting some files you have uploaded.';
$string['uploadfile'] =  'העלאת קובץ';
$string['uploadfileexistsoverwritecancel'] =  'A file with that name already exists.  You can rename the file you are about to upload, or overwrite the existing file.';
$string['uploadingfiletofolder'] =  'Uploading %s to %s';
$string['uploadoffilecomplete'] = 'Upload of %s complete';
$string['uploadoffilefailed'] =  'Upload of %s failed';
$string['uploadoffiletofoldercomplete'] = 'Upload of %s to %s complete';
$string['uploadoffiletofolderfailed'] = 'Upload of %s to %s failed';
$string['youmustagreetothecopyrightnotice'] = 'You must agree to the copyright notice';


// File types
$string['ai'] = 'Postscript Document';
$string['aiff'] = 'AIFF Audio File';
$string['application'] = 'Unknown Application';
$string['au'] = 'AU Audio File';
$string['avi'] = 'AVI Video File';
$string['bmp'] = 'Bitmap Image';
$string['doc'] = 'MS Word Document';
$string['dss'] = 'Digital Speech Standard Sound File';
$string['gif'] = 'GIF Image';
$string['html'] = 'HTML File';
$string['jpg'] = 'JPEG Image';
$string['jpeg'] = 'JPEG Image';
$string['js'] = 'Javascript File';
$string['latex'] = 'LaTeX Document';
$string['m3u'] = 'M3U Audio File';
$string['mp3'] = 'MP3 Audio File';
$string['mp4_audio'] = 'MP4 Audio File';
$string['mp4_video'] = 'MP4 Video File';
$string['mpeg'] = 'MPEG Movie';
$string['odb'] = 'Openoffice Database';
$string['odc'] = 'Openoffice Calc File';
$string['odf'] = 'Openoffice Formula File';
$string['odg'] = 'Openoffice Graphics File';
$string['odi'] = 'Openoffice Image';
$string['odm'] = 'Openoffice Master Document File';
$string['odp'] = 'Openoffice Presentation';
$string['ods'] = 'Openoffice Spreadsheet';
$string['odt'] = 'Openoffice Document';
$string['oth'] = 'Openoffice Web Document';
$string['ott'] = 'Openoffice Template Document';
$string['pdf'] = 'PDF Document';
$string['png'] = 'PNG Image';
$string['ppt'] = 'MS Powerpoint Document';
$string['quicktime'] = 'Quicktime Movie';
$string['ra'] = 'Real Audio File';
$string['rtf'] = 'RTF Document';
$string['sgi_movie'] = 'SGI Movie File';
$string['sh'] = 'Shell Script';
$string['tar'] = 'TAR Archive';
$string['gz'] = 'Gzip Compressed File';
$string['bz2'] = 'Bzip2 Compressed File';
$string['txt'] = 'Plain Text File';
$string['wav'] = 'WAV Audio File';
$string['wmv'] = 'WMV Video File';
$string['xml'] = 'XML File';
$string['zip'] = 'ZIP Archive';
$string['swf'] = 'SWF Flash movie';
$string['flv'] = 'FLV Flash movie';
$string['mov'] = 'MOV Quicktime movie';
$string['mpg'] = 'MPG Movie';
$string['ram'] = 'RAM Real Player Movie';
$string['rpm'] = 'RPM Real Player Movie';
$string['rm'] = 'RM Real Player Movie';


// Profile icons
$string['profileiconsize'] = 'Profile Icon Size';
$string['profileicons'] = 'Profile Icons';
$string['Default'] = 'Default';
$string['deleteselectedicons'] = 'Delete selected icons';
$string['profileicon'] = 'Profile Icon';
$string['noimagesfound'] = 'No images found';
$string['uploadedprofileiconsuccessfully'] = 'Uploaded new profile icon successfully';
$string['profileiconsetdefaultnotvalid'] = 'Could not set the default profile icon, the choice was not valid';
$string['profileiconsdefaultsetsuccessfully'] = 'Default profile icon set successfully';
$string['profileiconsdeletedsuccessfully'] = 'Profile icon(s) deleted successfully';
$string['profileiconsnoneselected'] = 'No profile icons were selected to be deleted';
$string['onlyfiveprofileicons'] = 'You may upload only five profile icons';
$string['or'] = 'or';
$string['profileiconuploadexceedsquota'] = 'Uploading this profile icon would exceed your disk quota. Try deleting some files you have uploaded';
$string['profileiconimagetoobig'] = 'The image you uploaded was too big (%sx%s pixels). It must not be larger than %sx%s pixels';
$string['uploadingfile'] = 'uploading file...';
$string['uploadprofileicon'] = 'Upload Profile Icon';
$string['profileiconsiconsizenotice'] = 'You may upload up to <strong>five</strong> profile icons here, and choose one to be displayed as your default icon at any one time. Your icons must be between 16x16 and %sx%s pixels in size.';
$string['setdefault'] = 'Set Default';
$string['Title'] = 'Title';
$string['imagetitle'] = 'Image Title';
$string['usenodefault'] = 'Use no default';
$string['usingnodefaultprofileicon'] = 'Now using no default profile icon';


?>
