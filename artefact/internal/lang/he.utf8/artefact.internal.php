<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'מאפיינים-אישיים';

$string['profile'] = 'מאפיינים אישיים';
$string['myfiles'] = 'הקבצים שלי';

$string['mandatory'] = 'חובה';
$string['public'] = 'ציבורי';

$string['aboutdescription'] = 'הזינו את שמכם האמיתי כאן. אם אתם מעוניינים להזדהות בשם אחר הזינו אותו בשדה הכינוי שלכם אשר יוצג באופן ציבורי';
$string['contactdescription'] = 'All of this information is private, unless you choose to put it in a view.';
$string['messagingdescription'] = 'Like your contact information, this information is private.';
$string['viewmyprofile'] = 'תצוגת המאפיינם האישיים שלי';

// profile categories
$string['aboutme'] = 'אודותי';
$string['contact'] = 'יצירת קשר';
$string['messaging'] = 'הודעות';
$string['general'] = 'כללי';

// profile fields
$string['firstname'] = 'שם פרטי';
$string['lastname'] = 'שם משפחה';
$string['fullname'] = 'שם מלא';
$string['institution'] = 'בית ספר';
$string['studentid'] = 'מספר תלמיד';
$string['preferredname'] = 'כינוי';
$string['introduction'] = 'תאור';
$string['email'] = 'כתובת דואר';
$string['officialwebsite'] = 'כתובת אתר רשמי';
$string['personalwebsite'] = 'כתובת אתר אישי';
$string['blogaddress'] = 'כתובת הבלוג';
$string['address'] = 'כתובת דואר חלזון';
$string['town'] = 'עיר';
$string['city'] = 'עיר/מחוז';
$string['country'] = 'מדינה';
$string['homenumber'] = 'טלפון בבית';
$string['businessnumber'] = 'Business Phone';
$string['mobilenumber'] = 'טלפון נייד';
$string['faxnumber'] = 'Fax Number';
$string['icqnumber'] = 'מספר ICQ';
$string['msnnumber'] = 'כינוי MSN';
$string['aimscreenname'] = 'AIM Screen Name';
$string['yahoochat'] = 'Yahoo Chat';
$string['skypeusername'] = 'משתמש SKYPE';
$string['jabberusername'] = 'Jabber Username';
$string['occupation'] = 'Occupation';
$string['industry'] = 'Industry';

// Field names for view user and search user display
$string['name'] = 'שם';
$string['principalemailaddress'] = 'דואר ראשי';
$string['emailaddress'] = 'דואר משני';

$string['saveprofile'] = 'Save Profile';
$string['profilesaved'] = 'Profile saved successfully';
$string['profilefailedsaved'] = 'Profile saving failed';


$string['emailvalidation_subject'] = 'Email validation';
$string['emailvalidation_body'] = <<<EOF
Hello %s,

The email address %s has been added to your user account in Mahara. Please visit the link below to activate this address.

%s
EOF;

$string['validationemailwillbesent'] = 'a validation email will be sent when you save your profile';
$string['emailactivation'] = 'Email Activation';
$string['emailactivationsucceeded'] = 'Email Activation Successful';
$string['emailactivationfailed'] = 'Email Activation Failed';
$string['unvalidatedemailalreadytaken'] = 'The e-mail address you are trying to validate is already taken';

$string['emailingfailed'] = 'Profile saved, but emails were not sent to: %s';

$string['loseyourchanges'] = 'Lose your changes?';

$string['editprofile']  = 'עריכת מאפיינים אישיים';
$string['Title'] = 'כותרת';

$string['Created'] = 'נוצר';
$string['Description'] = 'תאור';
$string['Download'] = 'הורדה';
$string['lastmodified'] = 'עודכן לאחרונה';
$string['Owner'] = 'בעלים';
$string['Preview'] = 'תצוגה מקדימה';
$string['Size'] = 'גודל';
$string['Type'] = 'סוג';

?>
