<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['changepassworddesc'] = 'If you wish to change your password, please enter the details here';
$string['changepasswordotherinterface'] = 'You may  <a href="%s">change your password</a> through a different interface</a>';
$string['oldpasswordincorrect'] = 'זו אינה ססימתכם הנוכחית';

$string['changeusernameheading'] = 'שינוי שם משתמש';
$string['changeusername'] = 'שם משתמש חדש';
$string['changeusernamedesc'] = 'The username you use to log into %s.  Usernames are 3-30 characters long, and may contain letters, numbers, and most common symbols excuding spaces.';

$string['accountoptionsdesc'] = 'ניתן לקבוע הדגרות חשבון כלליות כאן';
$string['friendsnobody'] = 'לא ניתו להוסיף אותי כחבר';
$string['friendsauth'] = 'חברים חדשים דורשים אישור שלי';
$string['friendsauto'] = 'חברים חדשים מורשים באופן מידי';
$string['friendsdescr'] = 'בקרת חברים';
$string['updatedfriendcontrolsetting'] = 'עדכון בקרת חברים';

$string['wysiwygdescr'] = 'מעבד תמלילים מתקדם';
$string['on'] = 'פעיל';
$string['off'] = 'כבוי';

$string['messagesdescr'] = 'הודעות ממשתמשים אחרים';
$string['messagesnobody'] = 'אף אחד אינו מורשה לשלוח אלי הודעות';
$string['messagesfriends'] = 'חברים שלי יכולים לשלוח אלי הודעות';
$string['messagesallow'] = 'כולם יכולים לשלוח אלי הודעות';

$string['language'] = 'שפה';

$string['showviewcolumns'] = 'הצגת מחוונים להוספה והסרה של עמודות בזמן עריכת תיק-התוצרים שלי';

$string['prefssaved']  = 'מאפיינים נשמרו';
$string['prefsnotsaved'] = 'Failed to save your Preferences!';

?>
