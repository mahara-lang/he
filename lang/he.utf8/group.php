<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// my groups
$string['groupname'] = 'שם קבוצה';
$string['creategroup'] = 'יצירת קבוצה';
$string['groupmemberrequests'] = 'בקשות הצטרפות ממתינות';
$string['sendinvitation'] = 'שליחת הזמנה';
$string['invitetogroupsubject'] = 'הנכם מוזמנים להצטרף לקבוצה';
$string['invitetogroupmessage'] = '%s has invited you to join a group, \'%s\'.  Click on the link below for more information.';
$string['inviteuserfailed'] = 'Failed to invite the user';
$string['userinvited'] = 'ההזמנה נשלחה';
$string['addedtogroupsubject'] = 'התווספתם לקבוצה';
$string['addedtogroupmessage'] = '%s הוסיף/ה אתכם לקבוצה, \'%s\'.  הקליקו על הקישור לצפייה בקבוצה';
$string['adduserfailed'] = 'שגיאה בתהליך ההוספה לקבוצה';
$string['useradded'] = 'המשתמש התווסף תקבוצה';
$string['editgroup'] = 'עריכת קבוצה';
$string['savegroup'] = 'שמירת מאפייני הקבוצה';
$string['groupsaved'] = 'מאפייני הקבוצה נשמרו בהצלחה';
$string['invalidgroup'] = 'הקבוצה אינה קיימת';
$string['canteditdontown'] = 'You can\'t edit this group because you don\'t own it';
$string['groupdescription'] = 'תאור הקבוצה';
$string['membershiptype'] = 'סוג החברות בקבוצה';
$string['membershiptype.controlled'] = 'חברות בהשגחה';
$string['membershiptype.invite']     = 'הזמנה בלבד';
$string['membershiptype.request']    = 'בקשת חברות';
$string['membershiptype.open']       = 'חברות פתוחה';
$string['pendingmembers']            = 'חברים בהמתנה';
$string['reason']                    = 'סיבה';
$string['approve']                   = 'מאושר';
$string['reject']                    = 'נדחה';
$string['groupalreadyexists'] = 'שם קבוצה זה קיים במערכת, נסו שם שונה';
$string['Created'] = 'נוצרה';
$string['groupadmins'] = 'מנהלי קבוצה';
$string['Admin'] = 'ניהול';
$string['grouptype'] = 'סוג קבוצה';
$string['publiclyviewablegroup'] = 'קבוצה הניתנת לצפייה ציבורית?';
$string['publiclyviewablegroupdescription'] = 'Allow anyone (including people who aren\'t members of this site) to view this group, including the forums?';
$string['usersautoadded'] = 'הוספת משתמשים באופן אוטומטי?';
$string['usersautoaddeddescription'] = 'הוספת משתמשים חדשים לקבוצה זו?';

$string['hasrequestedmembership'] = 'בקש/ה להצטרף כחבר/ה לקבוצה זו';
$string['hasbeeninvitedtojoin'] = 'הוזמן/ה להיות חבר/ה בקבוצה זו';
$string['groupinvitesfrom'] = 'הוזמנו להצטרף:';
$string['requestedmembershipin'] = 'בקשו חברות ב:';

// Used to refer to all the members of a group - NOT a "member" group role!
$string['member'] = 'חבר';
$string['members'] = 'חברים';
$string['Members'] = 'חברים';

$string['memberrequests'] = 'בקשת חברות';
$string['declinerequest'] = 'סרוב בקשת חברות';
$string['submittedviews'] = 'תיקי-תוצרים אשר נשלחו';
$string['releaseview'] = 'שחרור תיק-תוצרים';
$string['invite'] = 'הזמנה';
$string['remove'] = 'הסרה';
$string['updatemembership'] = 'עדכון חברות';
$string['memberchangefailed'] = 'Failed to update some membership information';
$string['memberchangesuccess'] = 'מצב בקשת חברות עודכן בהצלחה';
$string['viewreleasedsubject'] = 'דף-התוצרים שלכם פורסם';
$string['viewreleasedmessage'] = 'The view that you submitted to group %s has been released back to you by %s';
$string['viewreleasedsuccess'] = 'View was released successfully';
$string['groupmembershipchangesubject'] = 'Group membership: %s';
$string['groupmembershipchangedmessagetutor'] = 'You have been promoted to a tutor in this group';
$string['groupmembershipchangedmessagemember'] = 'You have been demoted from a tutor in this group';
$string['groupmembershipchangedmessageremove'] = 'You have been removed from this group';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Your request to join this group has been declined';
$string['groupmembershipchangedmessageaddedtutor'] = 'You have been added as a tutor in this group';
$string['groupmembershipchangedmessageaddedmember'] = 'You have been added as a member in this group';
$string['leavegroup'] = 'הסירו אותי מקבוצה זו';
$string['joingroup'] = 'צרפו אותי לקבוצה זו';
$string['requestjoingroup'] = 'בקשה לחברות בקבוצה זו';
$string['grouphaveinvite'] = 'הוזמנתם להצטרף כחברים לקבוצה זו';
$string['grouphaveinvitewithrole'] = 'You have been invited to join this group with the role';
$string['groupnotinvited'] = 'You have not been invited to join this group';
$string['groupinviteaccepted'] = 'בקשתכם לחברות בקבוצה זו אושרה! כעת, הנכם חברים בקבוצה';
$string['groupinvitedeclined'] = 'בקשתכם להצטרף כחברים לקבוצה נדחתה!';
$string['acceptinvitegroup'] = 'אישור חברות';
$string['declineinvitegroup'] = 'דחיית חברות';
$string['leftgroup'] = 'בטלתם את חברותכם בקבוצה';
$string['leftgroupfailed'] = 'Leaving group failed';
$string['couldnotleavegroup'] = 'You cannot leave this group';
$string['joinedgroup'] = 'כעת, הנכם חברים בקבוצה';
$string['couldnotjoingroup'] = 'אינכם יכולים להצטרף כחברים לקבוצה';
$string['grouprequestsent'] = 'בקשת הצטרפות כחברים לקבוצה נשלחה';
$string['couldnotrequestgroup'] = 'Could not send group membership request';
$string['cannotrequestjoingroup'] ='You cannot request to join this group';
$string['groupjointypeopen'] = 'Membership to this group is open. Feel free to join!';
$string['groupjointypecontrolled'] = 'Membership to this group  is controlled. You cannot join this group.';
$string['groupjointypeinvite'] = 'Membership to this group is by invitation only.';
$string['groupjointyperequest'] = 'Membership to this group is by request only.';
$string['grouprequestsubject'] = 'New group membership request';
$string['grouprequestmessage'] = '%s would like to join your group %s';
$string['grouprequestmessagereason'] = "%s would like to join your group %s. Their reason for wanting to join is:\n\n%s";
$string['cantdeletegroup'] = 'You cannot delete this group';
$string['groupconfirmdelete'] = 'Are you sure you wish to delete this group?';
$string['groupconfirmdeletehasviews'] = 'Are you sure you wish to delete this group? Some of your views use this group for access control, removing this group would mean that the members of that group would not have access to the views.';
$string['deletegroup'] = 'הקבוצה נמחקה בהצלחה';
$string['allmygroups'] = 'כל הקבוצות שלי';
$string['groupsimin']  = 'קבוצות בהן אני חבר/ה';
$string['groupsiown']  = 'קבוצות בבעלותי';
$string['groupsiminvitedto'] = 'קבוצות אליהן אני מוזמן/ת';
$string['groupsiwanttojoin'] = 'קבוצות אליהן אני מעוניין/ת להצטרף';
$string['therearependingrequests'] = 'There are %s pending membership requests for this group';
$string['thereispendingrequest'] = 'There is 1 pending membership request for this group';
$string['requestedtojoin'] = 'You have requested to join this group';
$string['groupnotfound'] = 'Group with id %s not found';
$string['groupconfirmleave'] = 'Are you sure you want to leave this group?';
$string['groupconfirmleavehasviews'] = 'Are you sure you want to leave this group? Some of your views use this group for access control, leaving this group would mean that the members of the group would not have access to the views';
$string['cantleavegroup'] = 'You can\'t leave this group';
$string['usercantleavegroup'] = 'This user cannot leave this group';
$string['usercannotchangetothisrole'] = 'The user cannot change to this role';
$string['leavespecifiedgroup'] = 'ביטול חברות בקבוצה \'%s\'';
$string['memberslist'] = 'חברים: ';
$string['nogroups'] = 'קבוצות לא קיימות';
$string['deletespecifiedgroup'] = 'מחיקת קבוצה \'%s\'';
$string['requestjoinspecifiedgroup'] = 'בקשת הצטרפות לקבוצה \'%s\'';
$string['youaregroupmember'] = 'הנכם חברים בקבוצה זו';
$string['youowngroup'] = 'הנכם הבעלים של קבוצה זו';
$string['groupsnotin'] = 'קבוצות בהן אינני חבר/ה';
$string['allgroups'] = 'כל הקבוצות';
$string['trysearchingforgroups'] = 'Try %ssearching for groups%s to join!';
$string['nogroupsfound'] = 'לא נמצאו קבוצות :(';
$string['group'] = 'קבוצה';
$string['Group'] = 'קבוצה';
$string['groups'] = 'קבוצות';
$string['notamember'] = 'אינכם חברים בקבוצה זו';
$string['notmembermayjoin'] = 'עליכם להיות חברים בקבוצה \'%s\' כדי לראות דף זה.';

// friendslist
$string['reasonoptional'] = 'סיבה (לא חובה)';
$string['request'] = 'בקשה';

$string['friendformaddsuccess'] = 'הוסיפו את %s לרשימת החברים שלכם';
$string['friendformremovesuccess'] = 'הסירו את %s מרשימת החברים שלכם';
$string['friendformrequestsuccess'] = 'שלחו בקשת חברות ל %s';
$string['friendformacceptsuccess'] = 'בקשת חברות אושרה';
$string['friendformrejectsuccess'] = 'בקשת חברות נדחתה';

$string['addtofriendslist'] = 'הוספה לרשימת החברים שלי';
$string['requestfriendship'] = 'בקשת חברות';

$string['addedtofriendslistsubject'] = 'חבר חדש';
$string['addedtofriendslistmessage'] = '%s added you as a friend! This means that %s is also on your friend list now too. '
    . ' Click on the link below to see their profile page';

$string['requestedfriendlistsubject'] = 'בקשת חבר חדשה';
$string['requestedfriendlistmessage'] = '%s בקש/ה שתוסיפו אותם כחברים שלכם.  '
    .' ניתן לבצע פעולה זו מהקישור הבא או מדף החברים שלכם במערכת ';

$string['requestedfriendlistmessagereason'] = '%s בקש/ה שתוסיפו אותם כחברים שלכם.  '
    .' ניתן לבצע פעולה זו מהקישור הבא או מדף החברים שלכם במערכת ';

$string['removefromfriendslist'] = 'הסרה מחברים';
$string['removefromfriends'] = 'הסרת %s מחברים';
$string['confirmremovefriend'] = 'האם אתם מעוניינים להסיר משתמש זה מרשימת החברים שלכם?';
$string['removedfromfriendslistsubject'] = 'הוסר/ה מרשימת החברים';
$string['removedfromfriendslistmessage'] = '%s הסיר/ה אתכם מרשימת החברים שלו/ה.';
$string['removedfromfriendslistmessagereason'] = '%s has removed you from their friends list.  Their reason was: ';
$string['cantremovefriend'] = 'You cannot remove this user from your friends list';

$string['friendshipalreadyrequested'] = 'You have requested to be added to %s\'s friends list';
$string['friendshipalreadyrequestedowner'] = '%s has requested to be added to your friends list';
$string['rejectfriendshipreason'] = 'Reason for rejecting request';

$string['friendrequestacceptedsubject'] = 'בקשת חברות התקבלה';
$string['friendrequestacceptedmessage'] = '%s אישור את בקשת החברות שלכם והוסיף/ה אתכם חברים שלו/ה'; 
$string['friendrequestrejectedsubject'] = 'בקשת החברות נדחתה';
$string['friendrequestrejectedmessage'] = '%s דחה/תה את בקשת החברות שלכם.';
$string['friendrequestrejectedmessagereason'] = '%s דחה/תה את בקשת החברות שלכם.  סיבת הדחייה הייתה: ';

$string['allfriends']     = 'כל החברים';
$string['currentfriends'] = 'חברים נוכחים';
$string['pendingfriends'] = 'חברים ממתינים לאישור';
$string['backtofriendslist'] = 'חזרה לרשימת החברים';
$string['findnewfriends'] = 'חיפוש חברים חדשים';
$string['Views']          = 'תיק-תוצרים';
$string['Files']          = 'קבצים';
$string['seeallviews']    = 'הצגת כל %s תיקיי-התוצרים...';
$string['noviewstosee']   = 'לא קיימים תיקיי-תוצרים לצפייה :(';
$string['whymakemeyourfriend'] = 'זו הסיבה אשר בגללה אני כדי לכם להיות חבר שלי:';
$string['approverequest'] = 'אישור בקשה!';
$string['denyrequest']    = 'סרוב בקשה';
$string['pending']        = 'מחכה לאישור';
$string['trysearchingforfriends'] = 'Try %ssearching for new friends%s to grow your network!';
$string['nobodyawaitsfriendapproval'] = 'Nobody is awaiting your approval to become your friend';
$string['sendfriendrequest'] = 'שליחת בקשת חברות!';
$string['addtomyfriends'] = 'הוספה לרשימת החברים שלי!';
$string['friendshiprequested'] = 'בקשת חברות נשלחה!';
$string['existingfriend'] = 'חבר קיים';
$string['nosearchresultsfound'] = 'לא נמצאו תוצאות בחיפוש :(';
$string['friend'] = 'חבר';
$string['friends'] = 'חברים';
$string['user'] = 'משתמש';
$string['users'] = 'משתנשים';
$string['Friends'] = 'חברים';

$string['friendlistfailure'] = 'Failed to modify your friends list';
$string['userdoesntwantfriends'] = 'משתמש זה אינו מעוניין בחברים חדשים';
$string['cannotrequestfriendshipwithself'] = 'איכם יכולים לבקש חברות מעצמכם';
$string['cantrequestfriendship'] = 'לא ניתן לבקש חברות עם משתמש זה';

// Messaging between users
$string['messagebody'] = 'שליחת הודעה'; // wtf
$string['sendmessage'] = 'שליחת הודעה';
$string['messagesent'] = 'ההודעה נשלחה!';
$string['messagenotsent'] = 'שגיאה בעת שליחת הודעה';
$string['newusermessage'] = 'הודעה חדשה מ %s';
$string['sendmessageto'] = 'ההודעה נשלחה אל %s';

$string['denyfriendrequest'] = 'דחיית בקשת חברות';
$string['sendfriendshiprequest'] = 'שליחת בקשת חברות ל %s';
$string['cantdenyrequest'] = 'זו אינה בקשת חברות תקינה';
$string['cantrequestfrienship'] = 'You cannot request frienship from this user';
$string['cantmessageuser'] = 'You cannot send this user a message';
$string['requestedfriendship'] = 'requested friendship';
$string['notinanygroups'] = 'לא מצויי באף קבוצה';
$string['addusertogroup'] = 'הוספה לקבוצה ';
$string['inviteusertojoingroup'] = 'הזמנה לקבוצה ';
$string['invitemembertogroup'] = 'הזמנת %s להצטרפות לקבוצה \'%s\'';
$string['cannotinvitetogroup'] = 'You can\'t invite this user to this group';
$string['useralreadyinvitedtogroup'] = 'This user has already been invited to, or is already a member of, this group.';
$string['removefriend'] = 'הסרת חבר';
$string['denyfriendrequestlower'] = 'דחיית בקשת חברות';

// Group interactions (activities)
$string['groupinteractions'] = 'פעילויות הקבוצה';
$string['nointeractions'] = 'לא קיימות פעילויות בקבוצה זו';
$string['notallowedtoeditinteractions'] = 'You are not allowed to add or edit activities in this group';
$string['notallowedtodeleteinteractions'] = 'You are not allowed to delete activities in this group';
$string['interactionsaved'] = '%s saved successfully';
$string['deleteinteraction'] = 'Delete %s \'%s\'';
$string['deleteinteractionsure'] = 'Are you sure you want to do this? It cannot be undone.';
$string['interactiondeleted'] = '%s deleted successfully';
$string['addnewinteraction'] = 'Add new %s';
$string['title'] = 'כותרת';
$string['Role'] = 'תפקיד';
$string['changerole'] = 'שינוי תפקיד';
$string['changeroleofuseringroup'] = 'שינוי תפקיד %s ב %s';
$string['currentrole'] = 'תפקיד נוכחי';
$string['changeroleto'] = 'שינוי תפקיד ל';
$string['rolechanged'] = 'התפקיד שונה';
$string['removefromgroup'] = 'הסרה מקבוצה';
$string['userremoved'] = 'המשתמש הוסר';
$string['About'] = 'אודות';

$string['Joined'] = 'הצטרף';
?>
