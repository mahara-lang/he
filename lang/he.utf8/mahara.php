<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

// General form strings
$string['add']     = 'הוספה';
$string['cancel']  = 'ביטול';
$string['delete']  = 'מחיקה';
$string['edit']    = 'עריכה';
$string['editing'] = 'עריכה';
$string['save']    = 'שמירה';
$string['submit']  = 'שליחה';
$string['update']  = 'עדכון';
$string['change']  = 'שינוי';
$string['send']    = 'שליחה';
$string['go']      = 'הצגה';
$string['default'] = 'בררת-מחדל';
$string['upload']  = 'העלאה';
$string['complete']  = 'הסתיים';
$string['Failed']  = 'נכשל';
$string['loading'] = 'בתהליך ביצוע ...';
$string['showtags'] = 'הצגת התוויות שלי';
$string['errorprocessingform'] = 'התגלתה תקלה בעת שליחת הטופס. אנא בדקו שוב שהזנתם באופן תקין את השדות המסומנים באדום';
$string['description'] = 'תאור';
$string['remove']  = 'הסרה';

$string['no']     = 'לא';
$string['yes']    = 'כן';
$string['none']   = 'ללא';
$string['at'] = 'ב';
$string['From'] = 'מ';
$string['To'] = 'ל';
$string['All'] = 'כל';

$string['next']      = 'הבא';
$string['nextpage']  = 'הדף הבא';
$string['previous']  = 'הקודם';
$string['prevpage']  = 'הדף הקודם';
$string['first']     = 'הראשון';
$string['firstpage'] = 'הדף הראשון';
$string['last']      = 'האחרון';
$string['lastpage']  = 'הדף האחרון';

$string['accept'] = 'אישור';
$string['reject'] = 'שלילה';
$string['sendrequest'] = 'שליחת בקשה';
$string['reason'] = 'סיבה';
$string['select'] = 'בחירה';

$string['tags'] = 'תויות';
$string['tagsdesc'] = 'הזינו תוויות מופרדות בפסיקים עבור פריט זה.';
$string['tagsdescprofile'] = 'Enter comma separated tags for this item. Items tagged with \'profile\' are displayed in your sidebar.';
$string['youhavenottaggedanythingyet'] = 'תרם יצרתם תוויות';

$string['selfsearch'] = 'חיפוש בתיק-העבודות שלי';

// Quota strings
$string['quota'] = 'מיכסה';
$string['quotausage'] = 'השתמשתם ב<span id="quota_used">%s</span> מתוך <span id="quota_total">%s</span> של מיכסתכם.';

$string['updatefailed'] = 'העלאת הקובץ לא הסתיימה בהצלחה';

$string['strftimenotspecified']  = 'לא מוגדר';

// profile sideblock strings
$string['invitedgroup'] = 'הקבוצה מוזמנת ל';
$string['invitedgroups'] = 'הקבוצות מוזמנות ל';
$string['logout'] = 'התנתקות';
$string['pendingfriend'] = 'חבר מחכה לאישור';
$string['pendingfriends'] = 'חברים מחכים לאישור';
$string['profile'] = 'תיק-אישי';
$string['views'] = 'תצוגות';

// Online users sideblock strings
$string['onlineusers'] = 'משתמשים מחוברים';
$string['lastminutes'] = 'ב %s הדקות האחרונות';

// Links and resources sideblock
$string['linksandresources'] = 'קישורים ומשאבים';

// auth
$string['accessforbiddentoadminsection'] = 'You are forbidden from accessing the administration section';
$string['accountdeleted'] = 'Sorry, your account has been deleted';
$string['accountexpired'] = 'Sorry, your account has expired';
$string['accountcreated'] = '%s: New account';
$string['accountcreatedtext'] = 'Dear %s,

A new account has been created for you on %s. Your details are as follows:

Username: %s
Password: %s

Visit %s to get started!

Regards, %s Site Administrator';
$string['accountcreatedchangepasswordtext'] = 'Dear %s,

A new account has been created for you on %s. Your details are as follows:

Username: %s
Password: %s

Once you log in for the first time, you will be asked to change your password.

Visit %s to get started!

Regards, %s Site Administrator';
$string['accountcreatedhtml'] = '<p>Dear %s</p>

<p>A new account has been created for you on <a href="%s">%s</a>. Your details are as follows:</p>

<ul>
    <li><strong>Username:</strong> %s</li>
    <li><strong>Password:</strong> %s</li>
</ul>

<p>Visit <a href="%s">%s</a> to get started!</p>

<p>Regards, %s Site Administrator</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>Dear %s</p>

<p>A new account has been created for you on <a href="%s">%s</a>. Your details are as follows:</p>

<ul>
    <li><strong>Username:</strong> %s</li>
    <li><strong>Password:</strong> %s</li>
</ul>

<p>Once you log in for the first time, you will be asked to change your password.</p>

<p>Visit <a href="%s">%s</a> to get started!</p>

<p>Regards, %s Site Administrator</p>
';
$string['accountexpirywarning'] = 'Account expiry warning';
$string['accountexpirywarningtext'] = 'Dear %s,

Your account on %s will expire within %s.

We recommend you save the contents of your portfolio using the Export tool. Instructions on using this feature may be found within the user guide.

If you wish to extend your account access or have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['accountexpirywarninghtml'] = '<p>Dear %s,</p>
    
<p>Your account on %s will expire within %s.</p>

<p>We recommend you save the contents of your portfolio using the Export tool. Instructions on using this feature may be found within the user guide.</p>

<p>If you wish to extend your account access or have any questions regarding the above, please feel free to <a href="%s">Contact Us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['institutionexpirywarning'] = 'Institution membership expiry warning';
$string['institutionexpirywarningtext'] = 'Dear %s,

Your membership of %s on %s will expire within %s.

If you wish to extend your membership or have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['institutionexpirywarninghtml'] = '<p>Dear %s,</p>
    
<p>Your membership of %s on %s will expire within %s.</p>

<p>If you wish to extend your membership or have any questions regarding the above, please feel free to <a href="%s">Contact Us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['accountinactive'] = 'Sorry, your account is currently inactive';
$string['accountinactivewarning'] = 'Account inactivity warning';
$string['accountinactivewarningtext'] = 'Dear %s,

Your account on %s will become inactive within %s.

Once inactive, you will not be able to log in until an administrator re-enables your account.

You can prevent your account from becoming inactive by logging in.

Regards, %s Site Administrator';
$string['accountinactivewarninghtml'] = '<p>Dear %s,</p>

<p>Your account on %s will become inactive within %s.</p>

<p>Once inactive, you will not be able to log in until an administrator re-enables your account.</p>

<p>You can prevent your account from becoming inactive by logging in.</p>

<p>Regards, %s Site Administrator</p>';
$string['accountsuspended'] = 'Your account has been suspeneded as of %s. The reason for your suspension is:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'Your account has been suspeneded';
$string['youraccounthasbeenunsuspended'] = 'Your account has been unsuspeneded';
$string['changepassword'] = 'Change Password';
$string['changepasswordinfo'] = 'You are required to change your password before you can proceed.';
$string['confirmpassword'] = 'Confirm password';
$string['javascriptnotenabled'] = 'Your browser does not have javascript enabled for this site. Mahara requires javascript to be enabled before you can log in';
$string['cookiesnotenabled'] = 'Your browser does not have cookies enabled, or is blocking cookies from this site. Mahara requires cookies to be enabled before you can log in';
$string['institution'] = 'מוסד';
$string['loggedoutok'] = 'התנתקתם מהמערכת בהצלחה';
$string['login'] = 'התחברות';
$string['loginfailed'] = 'You have not provided the correct credentials to log in. Please check your username and password are correct.';
$string['loginto'] = 'התחברות ל %s';
$string['newpassword'] = 'סיסמה חדשה';
$string['nosessionreload'] = 'רעננו את הדף לשם התחברות חדשה';
$string['oldpassword'] = 'סיסמה נוכחית';
$string['password'] = 'סיסמה';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = 'The password you use to access the system';
$string['passwordnotchanged'] = 'You did not change your password, please choose a new password';
$string['passwordsaved'] = 'Your new password has been saved';
$string['passwordsdonotmatch'] = 'The passwords do not match';
$string['passwordtooeasy'] = 'Your password is too easy! Please choose a harder password';
$string['register'] = 'Register';
$string['sessiontimedout'] = 'Your session has timed out, please enter your login details to continue';
$string['sessiontimedoutpublic'] = 'Your session has timed out. You may <a href="%s">log in</a> to continue browsing';
$string['sessiontimedoutreload'] = 'Your session has timed out. Reload the page to log in again';
$string['username'] = 'שם משתמש';
$string['preferredname'] = 'שם לתצוגה';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'The username you have been given to access this system.';
$string['youaremasqueradingas'] = 'שם הכינוי שלך הוא %s.';
$string['yournewpassword'] = 'סיסמתכם החדשה';
$string['yournewpasswordagain'] = 'אנא הזינו את סיסמתכם שוב';
$string['invalidsesskey'] = 'Invalid session key';
$string['cannotremovedefaultemail'] = 'You cannot remove your primary email address';
$string['emailtoolong'] = 'E-mail addresses cannot be longer that 255 characters';
$string['mustspecifyoldpassword'] = 'You must specify your current password';
$string['captchatitle'] = 'CAPTCHA תמונת';
$string['captchaimage'] = 'CAPTCHA תמונת';
$string['captchadescription'] = 'Enter the characters you see in the picture. Letters are not case sensitive';
$string['captchaincorrect'] = 'Enter the letters as they are shown in the image';
$string['Site'] = 'אתר';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = 'כתובת דואר';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'שם פרטי';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'שם משפחה';
$string['lastnamedescription'] = ' ';
$string['studentid'] = 'תעודת זהות';
$string['displayname'] = 'שם לתצוגה';
$string['fullname'] = 'שם מלא';
$string['registerstep1description'] = 'Welcome! To use this site you must first register. You must also agree to the <a href="terms.php">terms and conditions</a>. The data we collect here will be stored according to our <a href="privacy.php">privacy statement</a>.';
$string['registerstep3fieldsoptional'] = '<h3>Choose an Optional Profile Image</h3><p>You have now successfully registered with %s! You may now choose an optional profile icon to be displayed as your avatar.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>Fill Out Mandatory Profile Fields</h3><p>The following fields are required. You must fill them out before your registration is complete.</p>';
$string['registeringdisallowed'] = 'Sorry, you cannot register for this system at this time';
$string['membershipexpiry'] = 'Membership expires';
$string['institutionfull'] = 'The institution you have chosen is not accepting any more registrations.';
$string['registrationnotallowed'] = 'The institution you have chosen does not allow self-registration.';
$string['registrationcomplete'] = 'Thank you for registering at %s';
$string['language'] = 'שפה';

// Forgot password
$string['cantchangepassword'] = 'Sorry, you are unable to change your password through this interface - please use your institution\'s interface instead';
$string['forgotusernamepassword'] = 'Forgotten your username or password?';
$string['forgotusernamepasswordtext'] = '<p>If you have forgotten your username or password, enter the email address listed in your profile and we will send you a message you can use to give yourself a new password.</p>
<p>If you know your username and have forgotten your password, you can also enter your username instead.</p>';
$string['lostusernamepassword'] = 'Lost Username/Password';
$string['emailaddressorusername'] = 'Email address or username';
$string['pwchangerequestsent'] = 'You should receive an e-mail shortly with a link you can use to change the password for your account';
$string['forgotusernamepasswordemailsubject'] = 'Username/Password details for %s';
$string['forgotusernamepasswordemailmessagetext'] = 'Dear %s,

A username/password request has been made for your account on %s.

Your username is %s.

If you wish to reset your password, please follow the link below:

%s

If you did not request a password reset, please ignore this email.

If you have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Dear %s,</p>

<p>A username/password request has been made for your account on %s.</p>

<p>Your username is <strong>%s</strong>.</p>

<p>If you wish to reset your password, please follow the link below:</p>

<p><a href="%s">%s</a></p>

<p>If you did not request a password reset, please ignore this email.</p>

<p>If you have any questions regarding the above, please feel free to <a href="%s">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['forgotpassemailsendunsuccessful'] = 'Sorry, it appears that the e-mail could not be sent successfully. This is our fault, please try again shortly';
$string['forgotpassnosuchemailaddressorusername'] = 'The email address or username you entered doesn\'t match any users for this site';
$string['forgotpasswordenternew'] = 'אנא הזינו סיסמה חדשה כדי להמשיך';
$string['nosuchpasswordrequest'] = 'No such password request';
$string['passwordchangedok'] = 'סיסמתכם שונתה בהצלחה';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s: Membership of %s';
$string['noinstitutionsetpassemailmessagetext'] = 'Dear %s,

You are no longer a member of %s.
You may continue to use %s with your current username %s, but you must set a new password for your account.

Please follow the link below to continue the reset process.

%sforgotpass.php?key=%s

If you have any questions regarding the above, please feel free to contact
us.

%scontact.php

Regards, %s Site Administrator

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Dear %s,</p>

<p>You are no longer a member of %s.</p>
<p>You may continue to use %s with your current username %s, but you must set a new password for your account.</p>

<p>Please follow the link below to continue the reset process.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>If you have any questions regarding the above, please feel free to <a href="%scontact.php">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['debugemail'] = 'NOTICE: This e-mail was intended for %s <%s> but has been sent to you as per the "sendallemailto" configuration setting.';


// Expiry times
$string['noenddate'] = 'ללא תאריך סיום';
$string['day']       = 'יום';
$string['days']      = 'ימים';
$string['weeks']     = 'שבועות';
$string['months']    = 'חודשים';
$string['years']     = 'שנים';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = '%s מידע לא זמין';

// Contact us form
$string['name']                     = 'שם';
$string['email']                    = 'דואר';
$string['subject']                  = 'נושא';
$string['message']                  = 'הודעה';
$string['messagesent']              = 'הודעתכם נשלחה';
$string['nosendernamefound']        = 'לא צויין של השולח';
$string['emailnotsent']             = 'טופס יצירת קשר לא נשלח. תאור השגיאה: "%s"';

// mahara.js
$string['namedfieldempty'] = 'השדה המבוקש "%s" ריק';
$string['processing']     = 'מעדכן כעת';
$string['requiredfieldempty'] = 'שדה חיוני ללא תוכן';
$string['unknownerror']       = 'שגיאה לא ידועה  (0x20f91a0)';

// menu
$string['home']        = 'ראשי';
$string['myportfolio'] = 'תיק עבודות אישי';
$string['profile'] = 'מאפיינים אישיית';
$string['myviews']       = ' מצגות שלי';
$string['settings']    = 'מאפיינים';
$string['myfriends']          = 'החברים שלי';
$string['findfriends']        = 'חיפוש חברים';
$string['groups']             = 'קבוצות';
$string['mygroups']           = 'הקבוצות שלי';
$string['findgroups']         = 'חיפוש קבוצות';
$string['returntosite']       = 'חזרה לאתר';
$string['siteadministration'] = 'ניהול אתר';
$string['useradministration'] = 'ניהול משתמש';
$string['viewmyprofilepage']  = 'תצוגת דף פרטים-אישיים';
$string['editmyprofilepage']  = 'עריכת דף פרטים-אישיים';

$string['unreadmessages'] = 'הודעות שלא נקראו';
$string['unreadmessage'] = 'הודעה שלא נקראה';

$string['siteclosed'] = 'The site is temporarily closed for a database upgrade.  Site administrators may log in.';
$string['siteclosedlogindisabled'] = 'The site is temporarily closed for a database upgrade.  <a href="%s">Perform the upgrade now.</a>';

// footer
$string['termsandconditions'] = 'הנחיות ותנאים';
$string['privacystatement']   = 'הודעת פרטיות';
$string['about']              = 'אודות';
$string['contactus']          = 'צרו קשר';

// my account
$string['account'] =  'החשבון שלי';
$string['accountprefs'] = 'מאפיינים';
$string['preferences'] = 'מאפיינים';
$string['activityprefs'] = 'מאפייני פעילות';
$string['changepassword'] = 'שינוי סיסמה';
$string['notifications'] = 'הודעות';
$string['institutionmembership'] = 'חברות במוסד';
$string['institutionmembershipdescription'] = 'If you are a member of any institutions, they will be listed here.  You may also request membership of an institution, and if any institutions have invited you to join, you can accept or decline the invitation.';
$string['youareamemberof'] = 'הנכם חברים ב %s';
$string['leaveinstitution'] = 'Leave institution';
$string['reallyleaveinstitution'] = 'Are you sure you want to leave this institution?';
$string['youhaverequestedmembershipof'] = 'You have requested membership of %s';
$string['cancelrequest'] = 'Cancel request';
$string['youhavebeeninvitedtojoin'] = 'You have been invited to join %s';
$string['confirminvitation'] = 'Confirm invitation';
$string['joininstitution'] = 'Join institution';
$string['decline'] = 'Decline';
$string['requestmembershipofaninstitution'] = 'Request membership of an institution';
$string['optionalinstitutionid'] = 'Institution ID (optional)';
$string['institutionmemberconfirmsubject'] = 'Institution membership confirmation';
$string['institutionmemberconfirmmessage'] = 'You have been added as a member of %s.';
$string['institutionmemberrejectsubject'] = 'Institution membership request declined';
$string['institutionmemberrejectmessage'] = 'Your request for membership of %s was declined.';

$string['emailname'] = 'מערכת תיק-עבודות דיגיטלי'; // robot! 

$string['config'] = 'הגדרות';

$string['sendmessage'] = 'שליחת הודעה';

$string['notinstallable'] = 'Not installable!';
$string['installedplugins'] = 'Installed plugins';
$string['notinstalledplugins'] = 'Not installed plugins';
$string['plugintype'] = 'Plugin type';

$string['settingssaved'] = 'Settings saved';
$string['settingssavefailed'] = 'Failed to save settings';

$string['width'] = 'רוחב';
$string['height'] = 'גובה';
$string['widthshort'] = 'ר';
$string['heightshort'] = 'ג';
$string['filter'] = 'מסנן';
$string['expand'] = 'הרחבה';
$string['collapse'] = 'צמצום';
$string['more...'] = 'תוכן נוסף ...';
$string['nohelpfound'] = 'לא קיימת עזרה עבור פריט זה';
$string['nohelpfoundpage'] = 'לא קיימת עזרה עבור דף זה';
$string['couldnotgethelp'] = 'An error occurred trying to retrive the help page';
$string['profileimage'] = 'תמונת התיק-האישי';
$string['primaryemailinvalid'] = 'כתובת הדואר הראשית שלכם אינה תקינה';
$string['addemail'] = 'הוספת כתובת דואר';

// Search
$string['search'] = 'חיפוש';
$string['searchusers'] = 'חיפוש משתמשים';
$string['Query'] = 'חיפוש';
$string['query'] = 'חיפוש';
$string['querydescription'] = 'The words to be searched for';
$string['result'] = 'תוצאה';
$string['results'] = 'תוצאות';
$string['Results'] = 'תוצאות';
$string['noresultsfound'] = 'לא נמצאו תוצאות';
$string['users'] = 'משתמשים';

// artefact
$string['artefact'] = 'משאב';
$string['Artefact'] = 'משאב';
$string['Artefacts'] = 'משאבים';
$string['artefactnotfound'] = 'משאב בעל מספר סידורי %s לא נמצא';
$string['artefactnotrendered'] = 'משאב לא מוצג';
$string['nodeletepermission'] = 'You do not have permission to delete this artefact';
$string['noeditpermission'] = 'You do not have permission to edit this artefact';
$string['Permissions'] = 'הרשאות';
$string['republish'] = 'פרסום';
$string['view'] = 'תצוגה';
$string['artefactnotpublishable'] = 'המשאב %s לא פורסם בתצוגה %s';

$string['belongingto'] = 'שייך ל';
$string['allusers'] = 'כל המשתמשים';

// Upload manager
$string['quarantinedirname'] = 'הסגר';
$string['clammovedfile'] = 'הקובץ עבר לספריית הסגר.';
$string['clamdeletedfile'] = 'הקובץ נמחק';
$string['clamdeletedfilefailed'] = 'The file could not be deleted';
$string['clambroken'] = 'Your administrator has enabled virus checking for file uploads but has misconfigured something.  Your file upload was NOT successful. Your administrator has been emailed to notify them so they can fix it.  Maybe try uploading this file later.';
$string['clamemailsubject'] = '%s :: Clam AV notification';
$string['clamlost'] = 'Clam AV is configured to run on file upload, but the path supplied to Clam AV, %s, is invalid.';
$string['clamfailed'] = 'Clam AV has failed to run.  The return error message was %s. Here is the output from Clam:';
$string['clamunknownerror'] = 'There was an unknown error with clam.';
$string['image'] = 'תמונה';
$string['filenotimage'] = 'The file you uploaded is not valid image. It must be a PNG, JPEG or GIF file.';
$string['uploadedfiletoobig'] = 'The file was too big. Please ask your administrator for more information.';
$string['notphpuploadedfile'] = 'The file was lost in the upload process. This should not happen, please contact your administrator for more information.';
$string['virusfounduser'] = 'The file you have uploaded, %s, has been scanned by a virus checker and found to be infected! Your file upload was NOT successful.';
$string['fileunknowntype'] = 'The type of your uploaded file could not be determined. Your file may be corrupted, or it could be a configuration problem. Please contact your administrator.';
$string['virusrepeatsubject'] = 'Warning: %s is a repeat virus uploader.';
$string['virusrepeatmessage'] = 'The user %s has uploaded multiple files which have been scanned by a virus checker and found to be infected.';

$string['phpuploaderror'] = 'תקלה הופיע בעת העלאת הקובץ: %s (תקלה מספר %s)';
$string['phpuploaderror_1'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
$string['phpuploaderror_2'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
$string['phpuploaderror_3'] = 'The uploaded file was only partially uploaded.';
$string['phpuploaderror_4'] = 'No file was uploaded.';
$string['phpuploaderror_6'] = 'Missing a temporary folder.';
$string['phpuploaderror_7'] = 'Failed to write file to disk.';
$string['phpuploaderror_8'] = 'File upload stopped by extension.';
$string['adminphpuploaderror'] = 'A file upload error was probably caused by your server configuration.';

$string['youraccounthasbeensuspended'] = 'Your account has been suspended';
$string['youraccounthasbeensuspendedtext2'] = 'Your account at %s has been suspended by %s.'; // @todo: more info?
$string['youraccounthasbeensuspendedreasontext'] = "Your account at %s has been suspended by %s. Reason:\n\n%s";
$string['youraccounthasbeenunsuspended'] = 'Your account has been unsuspended';
$string['youraccounthasbeenunsuspendedtext2'] = 'Your account at %s has been unsuspended. You may once again log in and use the site.'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = 'Afghanistan';
$string['country.ax'] = 'Åland Islands';
$string['country.al'] = 'Albania';
$string['country.dz'] = 'Algeria';
$string['country.as'] = 'American Samoa';
$string['country.ad'] = 'Andorra';
$string['country.ao'] = 'Angola';
$string['country.ai'] = 'Anguilla';
$string['country.aq'] = 'Antarctica';
$string['country.ag'] = 'Antigua and Barbuda';
$string['country.ar'] = 'Argentina';
$string['country.am'] = 'Armenia';
$string['country.aw'] = 'Aruba';
$string['country.au'] = 'Australia';
$string['country.at'] = 'Austria';
$string['country.az'] = 'Azerbaijan';
$string['country.bs'] = 'Bahamas';
$string['country.bh'] = 'Bahrain';
$string['country.bd'] = 'Bangladesh';
$string['country.bb'] = 'Barbados';
$string['country.by'] = 'Belarus';
$string['country.be'] = 'Belgium';
$string['country.bz'] = 'Belize';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermuda';
$string['country.bt'] = 'Bhutan';
$string['country.bo'] = 'Bolivia';
$string['country.ba'] = 'Bosnia and Herzegovina';
$string['country.bw'] = 'Botswana';
$string['country.bv'] = 'Bouvet Island';
$string['country.br'] = 'Brazil';
$string['country.io'] = 'British Indian Ocean Territory';
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bg'] = 'Bulgaria';
$string['country.bf'] = 'Burkina Faso';
$string['country.bi'] = 'Burundi';
$string['country.kh'] = 'Cambodia';
$string['country.cm'] = 'Cameroon';
$string['country.ca'] = 'Canada';
$string['country.cv'] = 'Cape Verde';
$string['country.ky'] = 'Cayman Islands';
$string['country.cf'] = 'Central African Republic';
$string['country.td'] = 'Chad';
$string['country.cl'] = 'Chile';
$string['country.cn'] = 'China';
$string['country.cx'] = 'Christmas Island';
$string['country.cc'] = 'Cocos (Keeling) Islands';
$string['country.co'] = 'Colombia';
$string['country.km'] = 'Comoros';
$string['country.cg'] = 'Congo';
$string['country.cd'] = 'Congo, The Democratic Republic of The';
$string['country.ck'] = 'Cook Islands';
$string['country.cr'] = 'Costa Rica';
$string['country.ci'] = 'Cote D\'ivoire';
$string['country.hr'] = 'Croatia';
$string['country.cu'] = 'Cuba';
$string['country.cy'] = 'Cyprus';
$string['country.cz'] = 'Czech Republic';
$string['country.dk'] = 'Denmark';
$string['country.dj'] = 'Djibouti';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominican Republic';
$string['country.ec'] = 'Ecuador';
$string['country.eg'] = 'Egypt';
$string['country.sv'] = 'El Salvador';
$string['country.gq'] = 'Equatorial Guinea';
$string['country.er'] = 'Eritrea';
$string['country.ee'] = 'Estonia';
$string['country.et'] = 'Ethiopia';
$string['country.fk'] = 'Falkland Islands (Malvinas)';
$string['country.fo'] = 'Faroe Islands';
$string['country.fj'] = 'Fiji';
$string['country.fi'] = 'Finland';
$string['country.fr'] = 'France';
$string['country.gf'] = 'French Guiana';
$string['country.pf'] = 'French Polynesia';
$string['country.tf'] = 'French Southern Territories';
$string['country.ga'] = 'Gabon';
$string['country.gm'] = 'Gambia';
$string['country.ge'] = 'Georgia';
$string['country.de'] = 'Germany';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gr'] = 'Greece';
$string['country.gl'] = 'Greenland';
$string['country.gd'] = 'Grenada';
$string['country.gp'] = 'Guadeloupe';
$string['country.gu'] = 'Guam';
$string['country.gt'] = 'Guatemala';
$string['country.gg'] = 'Guernsey';
$string['country.gn'] = 'Guinea';
$string['country.gw'] = 'Guinea-bissau';
$string['country.gy'] = 'Guyana';
$string['country.ht'] = 'Haiti';
$string['country.hm'] = 'Heard Island and Mcdonald Islands';
$string['country.va'] = 'Holy See (Vatican City State)';
$string['country.hn'] = 'Honduras';
$string['country.hk'] = 'Hong Kong';
$string['country.hu'] = 'Hungary';
$string['country.is'] = 'Iceland';
$string['country.in'] = 'India';
$string['country.id'] = 'Indonesia';
$string['country.ir'] = 'Iran, Islamic Republic of';
$string['country.iq'] = 'Iraq';
$string['country.ie'] = 'Ireland';
$string['country.im'] = 'Isle of Man';
$string['country.il'] = 'ישראל';
$string['country.it'] = 'Italy';
$string['country.jm'] = 'Jamaica';
$string['country.jp'] = 'Japan';
$string['country.je'] = 'Jersey';
$string['country.jo'] = 'Jordan';
$string['country.kz'] = 'Kazakhstan';
$string['country.ke'] = 'Kenya';
$string['country.ki'] = 'Kiribati';
$string['country.kp'] = 'Korea, Democratic People\'s Republic of';
$string['country.kr'] = 'Korea, Republic of';
$string['country.kw'] = 'Kuwait';
$string['country.kg'] = 'Kyrgyzstan';
$string['country.la'] = 'Lao People\'s Democratic Republic';
$string['country.lv'] = 'Latvia';
$string['country.lb'] = 'Lebanon';
$string['country.ls'] = 'Lesotho';
$string['country.lr'] = 'Liberia';
$string['country.ly'] = 'Libyan Arab Jamahiriya';
$string['country.li'] = 'Liechtenstein';
$string['country.lt'] = 'Lithuania';
$string['country.lu'] = 'Luxembourg';
$string['country.mo'] = 'Macao';
$string['country.mk'] = 'Macedonia, The Former Yugoslav Republic of';
$string['country.mg'] = 'Madagascar';
$string['country.mw'] = 'Malawi';
$string['country.my'] = 'Malaysia';
$string['country.mv'] = 'Maldives';
$string['country.ml'] = 'Mali';
$string['country.mt'] = 'Malta';
$string['country.mh'] = 'Marshall Islands';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauritania';
$string['country.mu'] = 'Mauritius';
$string['country.yt'] = 'Mayotte';
$string['country.mx'] = 'Mexico';
$string['country.fm'] = 'Micronesia, Federated States of';
$string['country.md'] = 'Moldova, Republic of';
$string['country.mc'] = 'Monaco';
$string['country.mn'] = 'Mongolia';
$string['country.ms'] = 'Montserrat';
$string['country.ma'] = 'Morocco';
$string['country.mz'] = 'Mozambique';
$string['country.mm'] = 'Myanmar';
$string['country.na'] = 'Namibia';
$string['country.nr'] = 'Nauru';
$string['country.np'] = 'Nepal';
$string['country.nl'] = 'Netherlands';
$string['country.an'] = 'Netherlands Antilles';
$string['country.nc'] = 'New Caledonia';
$string['country.nz'] = 'New Zealand';
$string['country.ni'] = 'Nicaragua';
$string['country.ne'] = 'Niger';
$string['country.ng'] = 'Nigeria';
$string['country.nu'] = 'Niue';
$string['country.nf'] = 'Norfolk Island';
$string['country.mp'] = 'Northern Mariana Islands';
$string['country.no'] = 'Norway';
$string['country.om'] = 'Oman';
$string['country.pk'] = 'Pakistan';
$string['country.pw'] = 'Palau';
$string['country.ps'] = 'Palestinian Territory, Occupied';
$string['country.pa'] = 'Panama';
$string['country.pg'] = 'Papua New Guinea';
$string['country.py'] = 'Paraguay';
$string['country.pe'] = 'Peru';
$string['country.ph'] = 'Philippines';
$string['country.pn'] = 'Pitcairn';
$string['country.pl'] = 'Poland';
$string['country.pt'] = 'Portugal';
$string['country.pr'] = 'Puerto Rico';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'Romania';
$string['country.ru'] = 'Russian Federation';
$string['country.rw'] = 'Rwanda';
$string['country.sh'] = 'Saint Helena';
$string['country.kn'] = 'Saint Kitts and Nevis';
$string['country.lc'] = 'Saint Lucia';
$string['country.pm'] = 'Saint Pierre and Miquelon';
$string['country.vc'] = 'Saint Vincent and The Grenadines';
$string['country.ws'] = 'Samoa';
$string['country.sm'] = 'San Marino';
$string['country.st'] = 'Sao Tome and Principe';
$string['country.sa'] = 'Saudi Arabia';
$string['country.sn'] = 'Senegal';
$string['country.cs'] = 'Serbia and Montenegro';
$string['country.sc'] = 'Seychelles';
$string['country.sl'] = 'Sierra Leone';
$string['country.sg'] = 'Singapore';
$string['country.sk'] = 'Slovakia';
$string['country.si'] = 'Slovenia';
$string['country.sb'] = 'Solomon Islands';
$string['country.so'] = 'Somalia';
$string['country.za'] = 'South Africa';
$string['country.gs'] = 'South Georgia and The South Sandwich Islands';
$string['country.es'] = 'Spain';
$string['country.lk'] = 'Sri Lanka';
$string['country.sd'] = 'Sudan';
$string['country.sr'] = 'Suriname';
$string['country.sj'] = 'Svalbard and Jan Mayen';
$string['country.sz'] = 'Swaziland';
$string['country.se'] = 'Sweden';
$string['country.ch'] = 'Switzerland';
$string['country.sy'] = 'Syrian Arab Republic';
$string['country.tw'] = 'Taiwan, Province of China';
$string['country.tj'] = 'Tajikistan';
$string['country.tz'] = 'Tanzania, United Republic of';
$string['country.th'] = 'Thailand';
$string['country.tl'] = 'Timor-leste';
$string['country.tg'] = 'Togo';
$string['country.tk'] = 'Tokelau';
$string['country.to'] = 'Tonga';
$string['country.tt'] = 'Trinidad and Tobago';
$string['country.tn'] = 'Tunisia';
$string['country.tr'] = 'Turkey';
$string['country.tm'] = 'Turkmenistan';
$string['country.tc'] = 'Turks and Caicos Islands';
$string['country.tv'] = 'Tuvalu';
$string['country.ug'] = 'Uganda';
$string['country.ua'] = 'Ukraine';
$string['country.ae'] = 'United Arab Emirates';
$string['country.gb'] = 'United Kingdom';
$string['country.us'] = 'United States';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Uzbekistan';
$string['country.vu'] = 'Vanuatu';
$string['country.ve'] = 'Venezuela';
$string['country.vn'] = 'Viet Nam';
$string['country.vg'] = 'Virgin Islands, British';
$string['country.vi'] = 'Virgin Islands, U.S.';
$string['country.wf'] = 'Wallis and Futuna';
$string['country.eh'] = 'Western Sahara';
$string['country.ye'] = 'Yemen';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'מערכת';
$string['done'] = 'בוצע';
$string['back'] = 'חזרה';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['formatpostbbcode'] = 'ניתן לכתוב בשפת BBCode. %sמידע נוסף%s';

// import related strings (maybe separated later)
$string['importedfrom'] = 'Imported from %s';
$string['incomingfolderdesc'] = 'Files imported from other networked hosts';
$string['remotehost'] = 'Remote host %s';

$string['Copyof'] = 'העתק של %s';

// Profie views
$string['loggedinusersonly'] = 'משתמשים מחוברים בלבד';
$string['allowpublicaccess'] = 'לשימוש חופשי של כל הגולשים';
$string['thisistheprofilepagefor'] = 'זהו דף הפרטים-האישיים של משתמש %s';

$string['pleasedonotreplytothismessage'] = "אנא אל תגיבו להודעה זו.";
?>
