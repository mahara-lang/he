<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['createview']             = 'יצירת תיק-תוצרים';
$string['createviewstepone']      = 'יצירת תיק-תוצרים , שלב ראשון: תצורה כללית';
$string['createviewsteptwo']      = 'יצירת תיק-תוצרים , שלב שני: מאפיינים';
$string['createviewstepthree']    = 'יצירת תיק-תוצרים , שלב שלישי: בקרת גישה';
$string['createtemplate']         = 'יצירת תבנית';
$string['editviewdetails']        = 'עריכת מאפיינים עבור תיק-תוצרים "%s"';
$string['editblocksforview']      = 'עריכת תיק-תוצרים "%s"';
$string['editaccessforview']      = 'עריכת בקרת גישה עבור תיק-תוצרים "%s"';
$string['next']                   = 'הבא';
$string['back']                   = 'הקודם';
$string['title']                  = 'תצוגת כותרת';
$string['description']            = 'תצוגת תאור';
$string['startdate']              = 'תאריך/זמן תחילת גישה';
$string['stopdate']               = 'תאריך/זמן סיום גישה';
$string['startdatemustbebeforestopdate'] = 'על תאריך ההתחלה להיות לפני תאריך הסיום';
$string['ownerformat']            = 'Name display format';
$string['ownerformatdescription'] = 'How do you want people who look at your View to see your name?';
$string['profileviewtitle']       = 'תצוגת מאפיינים אישיים';
$string['editprofileview']        = 'עריכת מאפיינים אישיים';

// my views
$string['artefacts'] = 'משאבים';
$string['myviews'] = 'תיקי-התוצרים שלי';
$string['groupviews'] = 'תיקי-התוצרים של הקבוצה';
$string['institutionviews'] = 'תיקי-התוצרים של הארגון';
$string['reallyaddaccesstoemptyview'] = 'Your View contains no blocks.  Do you really want to give these users access to the View?';
$string['viewdeleted'] = 'תיק-תוצרים נמחק';
$string['viewsubmitted'] = 'תיק-תוצרים התווסף';
$string['editviewnameanddescription'] = 'עריכת מאפייני תיק-התוצרים';
$string['editviewaccess'] = 'עריכת בקרת גישה לתיק-תוצרים';
$string['deletethisview'] = 'מחיקת תיק-תוצרים זה';
$string['submitthisviewto'] = 'Submit this View to';
$string['forassessment'] = 'עבור משימה';
$string['accessfromdate2'] = 'Nobody else can see this View before %s';
$string['accessuntildate2'] = 'Nobody else can see this View after %s';
$string['accessbetweendates2'] = 'Nobody else can see this View before %s or after %s';
$string['artefactsinthisview'] = 'המשאבים בתיק-תוצרים זה';
$string['whocanseethisview'] = 'מי מורשה לראות תיק-תוצרים זה';
$string['view'] = 'תיק-תוצרים';
$string['views'] = 'תיקיי-תוצרים';
$string['View'] = 'תיק-תוצרים';
$string['Views'] = 'תיקיי-תוצרים';
$string['viewsubmittedtogroup'] = 'This View has been submitted to <a href="%sgroup/view.php?id=%s">%s</a>';
$string['nobodycanseethisview2'] = 'רק אתם יכולים לראות תיק-תוצרים זה';
$string['noviews'] = 'לא קיימים תיקיי-תוצרים.';
$string['youhavenoviews'] = 'לא יצרתם תיקיי-תוצרים, עדיין.';
$string['viewsownedbygroup'] = 'Views owned by this group';
$string['viewssharedtogroup'] = 'Views shared to this group';
$string['viewssharedtogroupbyothers'] = 'Views shared to this group by others';
$string['viewssubmittedtogroup'] = 'Views submitted to this group';

// access levels
$string['public'] = 'ציבורי';
$string['loggedin'] = 'משתמשים מחוברים';
$string['friends'] = 'חברים';
$string['groups'] = 'קבוצות';
$string['users'] = 'משתמשים';
$string['friendslower'] = 'חברים';
$string['grouplower'] = 'קבוצה';
$string['tutors'] = 'מדריכים';
$string['loggedinlower'] = 'משתמשים מחוברים';
$string['publiclower'] = 'ציבורי';
$string['everyoneingroup'] = 'כל אחד';
$string['token'] = 'כתובת אינטרנט חסוייה';

// view user
$string['inviteusertojoingroup'] = 'הזמנת משתמש זה להצטרף לקבוצה';
$string['addusertogroup'] = 'הוספת משתמש זה לקבוצה';

// view view
$string['addedtowatchlist'] = 'This View has been added to your watchlist';
$string['attachment'] = 'קובץ מצורף';
$string['removedfromwatchlist'] = 'This View has been removed from your watchlist';
$string['addfeedbackfailed'] = 'Add feedback failed';
$string['addtowatchlist'] = 'Add View to watchlist';
$string['removefromwatchlist'] = 'Remove View from watchlist';
$string['alreadyinwatchlist'] = 'This View is already in your watchlist';
$string['attachedfileaddedtofolder'] = "The attached file %s has been added to your '%s' folder.";
$string['attachfile'] = "קובץ מצורף";
$string['complaint'] = 'תלונה';
$string['date'] = 'תאריך';
$string['feedback'] = 'משוב';
$string['feedbackattachdirname'] = 'assessmentfiles';
$string['feedbackattachdirdesc'] = 'Files attached to View assessments';
$string['feedbackattachmessage'] = 'The attached file has been added to your %s folder';
$string['feedbackonthisartefactwillbeprivate'] = 'Feedback on this artefact will only be visible to the owner.';
$string['feedbackonviewbytutorofgroup'] = 'Feedback on %s by %s of %s';
$string['feedbacksubmitted'] = 'המשוב נשלח';
$string['makepublic'] = 'הפכו לציבורי';
$string['nopublicfeedback'] = 'אין משוב ציבורי';
$string['notifysiteadministrator'] = 'Notify site administrator';
$string['placefeedback'] = 'כתבו משוב';
$string['placefeedbacknotallowed'] = 'You are not allowed to place feedback on this View';
$string['print'] = 'הדפסה';
$string['thisfeedbackispublic'] = 'משוב זה הוא ציבורי';
$string['thisfeedbackisprivate'] = 'משוב זה הוא פרטי';
$string['makeprivate'] = 'Change to Private';
$string['reportobjectionablematerial'] = 'Report objectionable material';
$string['reportsent'] = 'Your report has been sent';
$string['updatewatchlistfailed'] = 'Update of watchlist failed';
$string['watchlistupdated'] = 'Your watchlist has been updated';
$string['editmyview'] = 'עריכת תיק-התוצרים שלי';
$string['backtocreatemyview'] = 'Back to create my View';

$string['friend'] = 'חבר';
$string['profileicon'] = 'תמונה אישית';

// general views stuff
$string['Added'] = 'התווסף';
$string['allviews'] = 'כל דפי התצוגה';

$string['submitviewconfirm'] = 'If you submit \'%s\' to \'%s\' for assessment, you will not be able to edit the View until your tutor has finished marking the View.  Are you sure you want to submit this View now?';
$string['viewsubmitted'] = 'תיק-תוצרים התווסף';
$string['submitviewtogroup'] = 'Submit \'%s\' to \'%s\' for assessment';
$string['cantsubmitviewtogroup'] = 'You cannot submit this View to this group for assessment';

$string['cantdeleteview'] = 'אינכם יכולים למחוק תיק-תוצרים זה';
$string['deletespecifiedview'] = 'מחיקת תיק-תוצרים "%s" ?';
$string['deleteviewconfirm'] = 'Do you really want to delete this View? It cannot be undone.';

$string['editaccesspagedescription2'] = '<p>By default, only you can see your View. Here you can choose who else you would like to be able to see the information in your View. Click Add to grant access to Public, Logged in Users or Friends. Use the search box to add individual Users or Groups. All those added will appear in the right hand pane, under Added.</p>
<p>You can also give other users permission to copy your View into their own portfolios.  When users copy a View, they will automatically get their own copies all of the files and folders within it.</p>
<p>Once you are done, scroll down and click Save to continue.</p>';

$string['overridingstartstopdate'] = 'Overriding Start/Stop Dates';
$string['overridingstartstopdatesdescription'] = 'If you want, you can set an overriding start and/or stop date. Other people will not be able to see your View before the start date and after the end date, regardless of any other access you have granted.';

$string['emptylabel'] = 'Click here to enter text for this label';
$string['empty_block'] = 'בחרו משאב מרשימת המשאבים השמאלית אשר יצורף לכאן';

$string['viewinformationsaved'] = 'מידע אודות תיק-התוצרים עודכן בהצלחה !';

$string['canteditdontown'] = 'You can\'t edit this View because you don\'t own it';
$string['canteditdontownfeedback'] = 'You can\'t edit this feedback because you don\'t own it';
$string['canteditsubmitted'] = 'You can\'t edit this View because it has been submitted for assessment to group "%s". You will have to wait until a tutor releases your view.';
$string['feedbackchangedtoprivate'] = 'Feedback changed to private';

$string['addtutors'] = 'Add Tutors';
$string['viewcreatedsuccessfully'] = 'View created successfully';
$string['viewaccesseditedsuccessfully'] = 'View access saved successfully';
$string['viewsavedsuccessfully'] = 'תיק-תוצרים נשמר בהצלחה';

$string['invalidcolumn'] = 'Column %s out of range';

$string['confirmcancelcreatingview'] = 'This View has not been completed. Do you really want to cancel?';

// view control stuff

$string['editblockspagedescription'] = '<p>Choose from the tabs below to see what blocks you can display in your View. You can drag and drop the blocks in to your View layout. Select the ? Icon for more information.</p>';
$string['displaymyview'] = 'הצגת תיק-התוצרים שלי';
$string['editthisview'] = 'עריכת תיק-תוצרים זה';

$string['success.addblocktype'] = 'Added block successfully';
$string['err.addblocktype'] = 'Could not add the block to your View';
$string['success.moveblockinstance'] = 'Moved block successfully';
$string['err.moveblockinstance'] = 'Could not move the block to the specified position';
$string['success.removeblockinstance'] = 'Deleted block successfully';
$string['err.removeblockinstance'] = 'Could not delete block';
$string['success.addcolumn'] = 'Added column successfully';
$string['err.addcolumn'] = 'Failed to add new column';
$string['success.removecolumn'] = 'Deleted column successfully';
$string['err.removecolumn'] = 'Failed to delete column';

$string['confirmdeleteblockinstance'] = 'Are you sure you wish to delete this block?';
$string['blockinstanceconfiguredsuccessfully'] = 'Block configured successfully';

$string['blocksintructionnoajax'] = 'Select a block and choose where to add it to your View. You can position a block using the arrow buttons in its titlebar';
$string['blocksinstructionajax'] = 'Drag blocks below this line to add them to your View layout. You can drag blocks around your View layout to position them.';

$string['addnewblockhere'] = 'הוספת משבצת חדשה כאן';
$string['add'] = 'הוספה';
$string['addcolumn'] = 'הוספת עמודה';
$string['removecolumn'] = 'הסרת עמודה זו';
$string['moveblockleft'] = 'העברת משבצת זו שמאלה';
$string['moveblockdown'] = 'העברת משבצת זו למטה';
$string['moveblockup'] = 'העברת משבצת זו למעלה';
$string['moveblockright'] = 'העברת משבצת זו ימינה';
$string['Configure'] = 'הגדרות';
$string['configureblock'] = 'הגדרות משבצת זו';
$string['removeblock'] = 'הסרת משבצת זו';
$string['blocktitle'] = 'כותרת המשבצת';

$string['changemyviewlayout'] = 'עדכון תתצורת תיק-התוצרים שלי';
$string['viewcolumnspagedescription'] = 'First, select the number of columns in your View. In the next step, you will be able to change the widths of the columns.';
$string['viewlayoutpagedescription'] = 'Select how you would like the columns in your View to be layed out.';
$string['changeviewlayout'] = 'שינוי מבנה תצורת תיק-התוצרים שלי';
$string['backtoyourview'] = 'חזרה לתיק-התוצרים שלי';
$string['viewlayoutchanged'] = 'תצורת מבנה דף תיק-התוצרים עודכנה';
$string['numberofcolumns'] = 'מספר עמודות';


$string['by'] = 'על ידי';
$string['in'] = 'ב';
$string['noblocks'] = 'Sorry, no blocks in this category :(';
$string['Preview'] = 'תצודה מקדימה';

$string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = 'רוחב זהה';
$string['67,33'] = 'עמודה שמאלית רחבה';
$string['33,67'] = 'עמודה ימנית רחבה';
$string['25,50,25'] = 'עמודה מרכזית רחבה';
$string['15,70,15'] = 'עמודה מרכזית רחבה מאוד';
$string['20,30,30,20'] = 'עמודות מרכזיות רחבות';
$string['noviewlayouts'] = 'There are no View layouts for a %s column View';

$string['blocktypecategory.feeds'] = 'הזנת חדשות חיצונית';
$string['blocktypecategory.fileimagevideo'] = 'קבצים, תמונות, סרטים';
$string['blocktypecategory.general'] = 'כללי';

$string['notitle'] = 'יש להזין כותרת';
$string['clickformoreinformation'] = 'Click for more information and to place feedback';

$string['Browse'] = 'עיון';
$string['Search'] = 'חיפוש';
$string['noartefactstochoosefrom'] = 'לא קיימים משאבים שמישים למשבצת זו';

$string['access'] = 'בקרת גישה';
$string['noaccesstoview'] = 'איכם מורשים לראות תיק-תוצרים זה';

// Templates
$string['Template'] = 'Template';
$string['allowcopying'] = 'ניתן להעתקה';
$string['templatedescription'] = 'Check this box if you would like the people who can see your view to be able to make their own copies of it.';
$string['choosetemplatepagedescription'] = '<p>Here you can search through the Views that you are allowed to copy as a starting point for making a new View. You can see a preview of each View by clicking on its name. Once you have found the View you wish to copy, click the corresponding "Copy View" button to make a copy and begin customising it.</p>';
$string['choosetemplategrouppagedescription'] = '<p>Here you can search through the Views that this group is allowed to copy as a starting point for making a new View. You can see a preview of each View by clicking on its name. Once you have found the View you wish to copy, click the corresponding "Copy View" button to make a copy and begin customising it.</p><p><strong>Note:</strong> Groups cannot currently make copies of Blogs or Blog Posts.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Here you can search through the Views that this institution is allowed to copy as a starting point for making a new View. You can see a preview of each View by clicking on its name. Once you have found the View you wish to copy, click the corresponding "Copy View" button to make a copy and begin customising it.</p><p><strong>Note:</strong> Institutions cannot currently make copies of Blogs or Blog Posts.</p>';
$string['copiedblocksandartefactsfromtemplate'] = 'Copied %d blocks and %d artefacts from %s';
$string['filescopiedfromviewtemplate'] = 'קבצים אשר הועתקו מ %s';
$string['viewfilesdirname'] = 'תצוגת קבצים';
$string['viewfilesdirdesc'] = 'קבצים מתיקי-תוצרים מועתקים';
$string['thisviewmaybecopied'] = 'יש אפשרות להעתיק';
$string['copythisview'] = 'העתקת תיק-תוצרים זה';
$string['copyview'] = 'העתקת תיק-תוצרים';
$string['createemptyview'] = 'יצירת תיק-תוצרים ריק';
$string['copyaview'] = 'העתקת תיק-תוצרים קיים';
$string['Untitled'] = 'ללא כותרת';
$string['copyfornewusers'] = 'העתקה עבור משתמשים חדשים';
$string['copyfornewusersdescription'] = 'Whenever a new user is created, automatically make a personal copy of this View in the user\'s portfolio.';
$string['copyfornewmembers'] = 'העתקה עבור חברי ארגון חדשים';
$string['copyfornewmembersdescription'] = 'Automatically make a personal copy of this View for all new members of %s.';
$string['copyfornewgroups'] = 'העתקה עבור קבוצות חדשות';
$string['copyfornewgroupsdescription'] = 'Make a copy of this view in all new groups with these Group Types:';
$string['searchviews'] = 'חיפוש תיקיי-תוצרים';
$string['searchowners'] = 'חיפוש בעלים';
$string['owner'] = 'בעלים';
$string['Owner'] = 'בעלים';
$string['owners'] = 'בעלים';
$string['show'] = 'הצגה';
$string['searchviewsbyowner'] = 'Search for Views by owner:';
$string['selectaviewtocopy'] = 'Select the View you wish to copy:';
$string['listviews'] = 'רשימת תיקיי-תוצרים';
$string['nocopyableviewsfound'] = 'No Views that you can copy';
$string['noownersfound'] = 'לא נמצאו בעלים';
$string['viewsby'] = 'תיקי-תוצרים של %s';
$string['Preview'] = 'תצוגה מקדימה';
$string['close'] = 'סיום';
$string['viewscopiedfornewusersmustbecopyable'] = 'You must allow copying before you can set a view to be copied for new users.';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'You must allow copying before you can set a view to be copied for new groups.';
$string['copynewusergroupneedsloggedinaccess'] = 'Views copied for new users or groups must give access to logged-in users.';

$string['blockcopypermission'] = 'הרשאות העתקה של משבצת';
$string['blockcopypermissiondesc'] = 'If you allow other users to copy this View, you may choose how this block will be copied';

?>
